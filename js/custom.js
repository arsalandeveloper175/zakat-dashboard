AOS.init();

  // Get titles from the DOM
  var titleMain  = $("#commentanimatedHeading");
  var titleSubs  = titleMain.find("slick-active");
  
  if (titleMain.length) {
  
  titleMain.slick({
    autoplay: false,
    arrows: false,
    dots: false,
    slidesToShow: 5,
    // centerPadding: "10px",
    draggable: false,
    infinite: true,
    pauseOnHover: false,
    swipe: false,
    touchMove: false,
    vertical: true,
    speed: 1000,
    autoplaySpeed: 2000,
    useTransform: true,
    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 1605,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 1367,
        settings: {
          slidesToShow: 3,
        }
      }
    ]
  });
  
  // On init
  $(".slick-dupe").each(function(index, el) {
    $("#commentanimatedHeading").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
  });
  
  // Manually refresh positioning of slick
  titleMain.slick('slickPlay');
  };
    

  $('.raised-amount-counter').easyPieChart({
    easing: false,
     barColor: '#461a55',
     rackColor: false,
     scaleColor: false,
     scaleLength: 3,
     lineCap: 'circle',
     lineWidth: '4',
     size: 87,
     trackColor: '#ccc'
 });



 var $post = $(".mock-screen");
setInterval(function(){
    $post.toggleClass("display");
}, 20000);

  setInterval(function(){ 
    // toggle the class every 
    $('.scroll-img-anim').addClass('active-anim');  
    $('.scroll-img-anim').removeClass('no-active-anim');  
    // setTimeout(function(){
    //   // toggle another class
    //   $('.scroll-img-anim').addClass('no-active-anim');  
    //   $('.scroll-img-anim').removeClass('active-anim');  
    // },10000)
 
 },10000);